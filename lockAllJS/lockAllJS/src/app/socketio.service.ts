import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import {MatTableDataSource} from '@angular/material';
import io from 'socket.io-client';

export class Client {
  name: string;
  mac: string;
  vote: boolean;

  constructor(name, mac, vote) {
    this.name = name;
    this.mac = mac;
    this.vote = vote;
  }
}

@Injectable()
export class SocketioService{
  socket;
  serverIp;
  serverPort;
  clients = [];
  currentClient: Client;
  connectedToServer = false;
  connectedAsClient = false;
  dataSourceClient;

  constructor(private electronService: ElectronService) { }

  initializeSocket(name) {
    this.electronService.ipcRenderer.send('ready_to_get');
    this.electronService.ipcRenderer.on('mac', (event, arg) => {
      this.currentClient = new Client(name, arg, false);
    });
  
    this.socket = io('http://' + this.serverIp + ':' + this.serverPort);

    this.socket.on('new_client_connected', () => {
      this.connectedToServer = true;
      this.connectedAsClient = true;
    });

    this.socket.on('new_client_not_connected', () => {
      this.socket.close();
    });

    this.socket.on('lock_all', () => {
      console.log('Locking Computer!');
      this.electronService.ipcRenderer.send('lock');
    });

    this.socket.on('client_list', (clients) => {
      let clientList = [];
      for (var c in clients) {
          let newClient = new Client(c, clients[c]['mac'], clients[c]['vote'])
          clientList.push(newClient);
		    }  
      this.clients = clientList;
      this.dataSourceClient = new MatTableDataSource(this.clients);
    });

    this.electronService.ipcRenderer.on('idle_reached', (event, arg) => {
      if (this.connectedAsClient) {
        this.disconnectUser();
      }
      
    });

    this.electronService.ipcRenderer.on('idle_okay', (event, arg) => {
      if (!this.connectedAsClient) {
        this.connectToServer();
      }
    });

    this.electronService.ipcRenderer.on('close_disconnect', (event, arg) => {
      this.disconnectUser();
      this.socket.close();
    });

  }

  connectToServer() {
    this.socket.emit('new_client', {'name': this.currentClient.name, 'mac': this.currentClient.mac});
  }

  toggleLock() {
    this.socket.emit('lock_all', this.currentClient.name);
  }

  disconnectUser() {
    this.socket.emit('disconnect_user', this.currentClient.name);
    this.connectedAsClient = false;
  }

}
