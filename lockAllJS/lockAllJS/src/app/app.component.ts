import { Component } from '@angular/core';
import { SocketioService } from './socketio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'SocketIo Appd';
  name;
  dataSource;
  displayedColumns = ['name', 'mac', 'vote'];

  constructor(public socketio: SocketioService) { }

  connectToServer() {
    this.socketio.initializeSocket(this.name);
    this.socketio.connectToServer();
  }

  toggleLock() {
    this.socketio.toggleLock();
  }

}
