const { app, BrowserWindow, ipcMain, Menu, MenuItem, Tray } = require('electron')
var systemIdleTime = require('@paulcbetts/system-idle-time');
var exec = require('child_process').exec;
var winLockCommand = 'rundll32.exe user32.dll, LockWorkStation';

let win;
let idleInterval;
const MINIMUM_IDLE_TIME_TO_BE_CONNECTED = 1000 * 60 * 3;

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800, 
    height: 800,
    backgroundColor: '#ffffff',
    icon: `./lock.ico`
  })

  win.loadURL(`file://${__dirname}/dist/index.html`)

  ipcMain.on('lock', (event, arg) => {  
    exec(winLockCommand);
  });

  idleInterval = setInterval(function(){
      if (systemIdleTime.getIdleTime() > MINIMUM_IDLE_TIME_TO_BE_CONNECTED) {
        win.webContents.send('idle_reached');
      } else {
        win.webContents.send('idle_okay');
      }
  }, 1000);

  
  require('getmac').getMac(function(err,macAddress){
    if (err)  throw err
    ipcMain.on('ready_to_get', (event, arg) => {  
      win.webContents.send('mac', macAddress);
    });
  })

  //////////
  // Tray //
  //////////
  var appIcon = new Tray(`./lock.ico`);
  var templateTray = Menu.buildFromTemplate([
    {
      label: 'Show',
      click:  function(){
        win.show();
      }
    },
    {
       label: 'Quit',
       click:  function(){
        app.isQuiting = true;
        app.quit();
  
      } 
    }
  ])
  appIcon.setToolTip('lockAll');
  appIcon.setContextMenu(templateTray);

  win.on('minimize',function(event){
    event.preventDefault()
      win.hide();
  });


  win.on('close', function (event) {
      if(!app.isQuiting){
          event.preventDefault()
          win.hide();
      }
      return false;
  });
  /////////
}

// Create window on electron intialization
app.on('ready', createWindow)

app.on('quit', () => {
  clearInterval(idleInterval);
  win.webContents.send('close_disconnect');
  win = null;
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow()
  }
})

const template = [
  {
     label: 'Tools',
     submenu: [
        {
           label: 'DevTools',
           accelerator: 'F12',
           click: (item, focusedWindow) => {
              focusedWindow.toggleDevTools();
          }
        }
     ]
  },
]


const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)

