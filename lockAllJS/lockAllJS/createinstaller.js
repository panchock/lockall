const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  })

function getInstallerConfig () {
  console.log('creating windows installer')
  const rootPath = path.join('./')
  const outPath = path.join(rootPath, 'release-builds')

  return Promise.resolve({
    appDirectory: path.join(outPath, 'lockAll-win32-x64/'),
    authors: 'Nice',
    noMsi: true,
    outputDirectory: path.join(outPath, 'windows-installer'),
    exe: 'lockAll.exe',
    setupExe: 'LockAllInstaller.exe',
    setupIcon: path.join(rootPath, 'dist', 'assets', 'lock.ico')
  })
}