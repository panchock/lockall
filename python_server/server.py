from flask import Flask, render_template
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, ping_timeout=120)

clients = {}
MINIMUM_CLIENTS_TO_LOCK = 2

@socketio.on('disconnect_user')
def disconnect_user(client_name):
    try:
        del clients[client_name]
        update_client_list()
    except:
        pass

@socketio.on('new_client')
def new_client(client_details):
    client_name = client_details['name']
    client_mac = client_details['mac']
    # Check if client already exist
    for client_name_key, client_details in clients.iteritems():
        if client_details['mac'] == client_mac or client_name_key == client_name:
            emit("new_client_not_connected")
            return
    clients[client_name] = {'vote' : False, 'mac': client_mac}
    emit("new_client_connected")
    update_client_list()

@socketio.on('lock_all')
def lock_all(client_name):
    clients[client_name]['vote'] = not clients[client_name]['vote']
    update_client_list()
    if locking_check():
        lock()


def locking_check():
    voted_clients = 0
    for client_name, client_details in clients.iteritems():
        if client_details['vote'] == True:
            voted_clients += 1
    if voted_clients >= MINIMUM_CLIENTS_TO_LOCK:
        return True
    return False

def lock():
    for key, value in clients.iteritems():
        value["vote"] = False
    emit('lock_all' , broadcast=True)
    update_client_list()

def update_client_list():
    emit('client_list', clients, broadcast=True)

if __name__ == '__main__':
    socketio.run(app)