var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

var clients = {}
var MINIMUM_CLIENTS_TO_LOCK = 2

io.on('connection', function(socket){
	console.log('[+] New Client Is Trying To Connect...')

	function update_client_list() {
		io.emit('client_list', clients, {for : 'everyone'})			
	}

	function locking_check() {
	    var voted_clients = 0
	    for (var c in clients) {
	        if (clients[c]['vote'] == true)
	            voted_clients += 1
	    }
	    if (voted_clients >= MINIMUM_CLIENTS_TO_LOCK)
	        return true
	    return false
	}

	function lock() {
		console.log('[!] Locking All')		
	    for (var c in clients) {
        	clients[c]['vote'] = false
        }
	    io.emit('lock_all', {for : 'everyone'})
	    update_client_list()
	}


	socket.on('new_client', function(client_details){
		var client_name = client_details['name']
		var client_mac = client_details['mac']

		// Check if client already exist
		for (var c in clients) {
		    if (clients[c]['mac'] == client_mac || c == client_name) {
		    	socket.emit('new_client_not_connected')
		        return
		    }  
		}
		console.log('[+] New Client: ' + client_name + ' mac: ' + client_mac)
		clients[client_name] = {'vote' : false, 'mac': client_mac}
		socket.emit('new_client_connected')
		update_client_list()
	});

	socket.on('disconnect_user', function(client_name){
		console.log('[+] Disconnect User: ' + client_name)
		delete clients[client_name]
		update_client_list()
	});

	socket.on('lock_all', function(client_name){
		console.log('[+] Client: ' + client_name + ' Voted To Lock')
	    clients[client_name]['vote'] = !clients[client_name]['vote']
	    update_client_list()
	    if (locking_check())
	        lock()
	});

	socket.on('disconnect', function(){
		console.log('[+] Client disconnected');
	});
});

http.listen(5000, function(){
  console.log('listening on *:5000')
});