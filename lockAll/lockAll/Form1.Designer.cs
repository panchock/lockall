﻿namespace lockAll
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.lstClients = new System.Windows.Forms.DataGridView();
            this.mynotifyicon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tmrIdleTime = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtServerIp = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtServerPort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnConnect = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnToggleVote = new MaterialSkin.Controls.MaterialRaisedButton();
            ((System.ComponentModel.ISupportInitialize)(this.lstClients)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Gisha", 11.25F);
            this.label1.Location = new System.Drawing.Point(225, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // lstClients
            // 
            this.lstClients.AllowUserToAddRows = false;
            this.lstClients.AllowUserToDeleteRows = false;
            this.lstClients.AllowUserToResizeColumns = false;
            this.lstClients.AllowUserToResizeRows = false;
            this.lstClients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.lstClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstClients.Location = new System.Drawing.Point(0, 282);
            this.lstClients.Name = "lstClients";
            this.lstClients.ReadOnly = true;
            this.lstClients.Size = new System.Drawing.Size(717, 259);
            this.lstClients.TabIndex = 4;
            // 
            // mynotifyicon
            // 
            this.mynotifyicon.Icon = ((System.Drawing.Icon)(resources.GetObject("mynotifyicon.Icon")));
            this.mynotifyicon.Text = "notifyIcon1";
            this.mynotifyicon.Visible = true;
            this.mynotifyicon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mynotifyicon_MouseDoubleClick);
            // 
            // tmrIdleTime
            // 
            this.tmrIdleTime.Interval = 10000;
            this.tmrIdleTime.Tick += new System.EventHandler(this.tmrIdleTime_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Gisha", 11.25F);
            this.label2.Location = new System.Drawing.Point(156, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Server Ip:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Gisha", 11.25F);
            this.label3.Location = new System.Drawing.Point(407, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Port:";
            // 
            // txtServerIp
            // 
            this.txtServerIp.BackColor = System.Drawing.Color.White;
            this.txtServerIp.Depth = 0;
            this.txtServerIp.Hint = "";
            this.txtServerIp.Location = new System.Drawing.Point(228, 86);
            this.txtServerIp.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServerIp.Name = "txtServerIp";
            this.txtServerIp.PasswordChar = '\0';
            this.txtServerIp.SelectedText = "";
            this.txtServerIp.SelectionLength = 0;
            this.txtServerIp.SelectionStart = 0;
            this.txtServerIp.Size = new System.Drawing.Size(158, 23);
            this.txtServerIp.TabIndex = 9;
            this.txtServerIp.Text = "10.0.0.160";
            this.txtServerIp.UseSystemPasswordChar = false;
            // 
            // txtServerPort
            // 
            this.txtServerPort.BackColor = System.Drawing.Color.White;
            this.txtServerPort.Depth = 0;
            this.txtServerPort.Hint = "";
            this.txtServerPort.Location = new System.Drawing.Point(451, 86);
            this.txtServerPort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.PasswordChar = '\0';
            this.txtServerPort.SelectedText = "";
            this.txtServerPort.SelectionLength = 0;
            this.txtServerPort.SelectionStart = 0;
            this.txtServerPort.Size = new System.Drawing.Size(75, 23);
            this.txtServerPort.TabIndex = 10;
            this.txtServerPort.Text = "5000";
            this.txtServerPort.UseSystemPasswordChar = false;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.Depth = 0;
            this.txtName.Hint = "";
            this.txtName.Location = new System.Drawing.Point(282, 118);
            this.txtName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtName.Name = "txtName";
            this.txtName.PasswordChar = '\0';
            this.txtName.SelectedText = "";
            this.txtName.SelectionLength = 0;
            this.txtName.SelectionStart = 0;
            this.txtName.Size = new System.Drawing.Size(143, 23);
            this.txtName.TabIndex = 11;
            this.txtName.Text = "1";
            this.txtName.UseSystemPasswordChar = false;
            // 
            // btnConnect
            // 
            this.btnConnect.AutoSize = true;
            this.btnConnect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnConnect.Depth = 0;
            this.btnConnect.Location = new System.Drawing.Point(311, 159);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnConnect.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Primary = false;
            this.btnConnect.Size = new System.Drawing.Size(75, 36);
            this.btnConnect.TabIndex = 12;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnToggleVote
            // 
            this.btnToggleVote.Depth = 0;
            this.btnToggleVote.Enabled = false;
            this.btnToggleVote.Location = new System.Drawing.Point(282, 204);
            this.btnToggleVote.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnToggleVote.Name = "btnToggleVote";
            this.btnToggleVote.Primary = true;
            this.btnToggleVote.Size = new System.Drawing.Size(132, 72);
            this.btnToggleVote.TabIndex = 14;
            this.btnToggleVote.Text = "Toggle Vote";
            this.btnToggleVote.UseVisualStyleBackColor = true;
            this.btnToggleVote.Click += new System.EventHandler(this.btnToggleVote_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 537);
            this.Controls.Add(this.btnToggleVote);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtServerPort);
            this.Controls.Add(this.txtServerIp);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstClients);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "lockAll";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.lstClients)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView lstClients;
        private System.Windows.Forms.NotifyIcon mynotifyicon;
        private System.Windows.Forms.Timer tmrIdleTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtServerIp;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtServerPort;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtName;
        private MaterialSkin.Controls.MaterialFlatButton btnConnect;
        private MaterialSkin.Controls.MaterialRaisedButton btnToggleVote;
    }
}

