﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using MaterialSkin.Controls;
using MaterialSkin;

namespace lockAll
{
    public partial class Form1 : MaterialForm
    {
        const int MINIMUM_CLIENTS_TO_VOTE = 2;
        Socket socket;
        const int MINIMUM_IDLE_TIME = 1000 * 60 * 3;
        bool connected = false;

        public Form1()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
        }

        private void initializeSocket()
        {

            socket.On("client_list", (data) =>
            {

                JObject dataA = (JObject)data;
                List<User> clients = new List<User>();

                // Check clients count
                if (dataA.Count >= MINIMUM_CLIENTS_TO_VOTE)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        btnToggleVote.Enabled = true;
                    });
                }
                else
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        btnToggleVote.Enabled = false;
                    });
                }


                foreach (var sub_obj in dataA)
                {
                    JToken value = sub_obj.Value;
                    clients.Add(new User() { clientName = sub_obj.Key, Mac = value["mac"].ToString(), Vote = value["vote"].ToObject<bool>() });
                }

                this.Invoke((MethodInvoker)delegate
                {
                    var bindingList = new BindingList<User>(clients);
                    lstClients.DataSource = bindingList;
                });
            });

            socket.On("lock_all", () =>
            {
                System.Diagnostics.Process.Start(@"C:\WINDOWS\system32\rundll32.exe", "user32.dll,LockWorkStation");
            });
            
        }

        /*
         * Gets the mac address of the matching interface (same subnet with the server)
         */
        private string GetMacAddress()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            string ipString = ip.Address.ToString();
                            if (ipString.StartsWith(txtServerIp.Text.Substring(0, 3)))
                            {
                                string macAddress = ni.GetPhysicalAddress().ToString();
                                string macAddressSplitted = Regex.Replace(macAddress, ".{2}", "$0:");
                                return macAddressSplitted.Substring(0, macAddressSplitted.Length - 1);
                            }
                        }
                    }
                }
            }
            return "00:00:00:00:00:00";
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                mynotifyicon.Visible = true;
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                mynotifyicon.Visible = false;
            }

        }

        private void mynotifyicon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mynotifyicon.Visible = false;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (socket != null)
            {
                socket.Emit("disconnect_user", txtName.Text);
                socket.Close();
                Thread.Sleep(1000);
            }
        }

        private void tmrIdleTime_Tick(object sender, EventArgs e)
        {
            uint idleTime = IdleTimeFinder.GetIdleTime();
            if (idleTime > MINIMUM_IDLE_TIME)
            {
                if (connected)
                {
                    socket.Emit("disconnect_user", txtName.Text);
                    connected = false;
                }
                
            }
            else  
            {
                if (!connected)
                {
                    socket.Emit("new_client", JObject.FromObject(new { name = txtName.Text, mac = GetMacAddress() }));
                }
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            socket = IO.Socket("http://" + txtServerIp.Text + ":" + txtServerPort.Text);
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                initializeSocket();
                socket.Emit("new_client", JObject.FromObject(new { name = txtName.Text, mac = GetMacAddress() }));
                socket.On("new_client_connected", () =>
                {
                    connected = true;
                    this.Invoke((MethodInvoker)delegate
                    {
                        txtName.Enabled = false;
                        btnConnect.Enabled = false;
                        txtServerIp.Enabled = false;
                        txtServerPort.Enabled = false;
                        tmrIdleTime.Start();
                    });
                });

                socket.On("new_client_not_connected", () =>
                {
                    socket.Close();
                });
            });
        }

        private void btnToggleVote_Click(object sender, EventArgs e)
        {
            socket.Emit("lock_all", txtName.Text);
        }

    }
}
